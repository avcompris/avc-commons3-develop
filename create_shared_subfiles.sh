#!/bin/sh

# File: avc-commons3-develop/create_shared_subfiles.sh
#
# Parameters:
#    SHARED        e.g. avc-examples3-shared -- Must be the current directory
#    GROUP_ID      e.g. net.avcompris.commons
#    VERSION       e.g. 0.0.1-SNAPSHOT
#    GITLAB_GROUP  e.g. avcompris
#    PACKAGE_BASE  e.g. net.avcompris.examples.shared
#
# Create the following subdirs and files in a "xxx-shared" directory:
#
#   .
#                       pom.xml <-- will inherit from "avc-commons3-component"
#   xxx-shared-notifier
#                       src/main/java/${package_base_path}/notifier
#                       src/test/java/${package_base_path}/notifier
#                       pom.xml
#   xxx-shared-dao-RDS
#                       src/main/java/${package_base_path}/dao/impl
#                       src/test/java/${package_base_path}/dao/impl
#                       pom.xml
#   xxx-shared-api
#                       src/main/java/${package_base_path}/api/exception
#                       src/test/java/${package_base_path}/api/exception
#                       pom.xml
#   xxx-shared-core-api
#                       src/main/java/${package_base_path}/core/api
#                       src/test/java/${package_base_path}/core/api
#                       pom.xml
#   xxx-shared-core
#                       src/main/java/${package_base_path}/core/impl
#                       src/test/java/${package_base_path}/core/impl
#                       pom.xml
#   xxx-shared-core-tests
#                       src/main/java/${package_base_path}/core/tests
#                       src/test/java/${package_base_path}/core/tests
#                       pom.xml
#

set -e

#-------------------------------------------------------------------------------
#   0. DEFS
#-------------------------------------------------------------------------------

error() {
	message="${1}"
	echo "${message}" >&2
	echo "Exiting." >&2
	exit 1
}

filter_file() {

	subfile_src="${1}"

	cat "${subfile_src}" \
		| sed s/\$\{SHARED\}/${SHARED}/ \
		| sed s/\$\{GROUP_ID\}/${GROUP_ID}/ \
		| sed s/\$\{VERSION\}/${VERSION}/ \
		| sed s/\$\{GITLAB_GROUP\}/${GITLAB_GROUP}/ \
		| sed s/\$\{PACKAGE_BASE\}/${PACKAGE_BASE}/
}

#-------------------------------------------------------------------------------
#   2. EXTRACT SHARED NAME
#-------------------------------------------------------------------------------

SHARED=`basename "${PWD}"`

if [ -z "${SHARED}" ]; then
	error "*** ERROR *** Cannot extract SHARED name: `pwd`"
fi

SUFFIX=`echo "${SHARED}" | sed s/^.*-//` # "shared"

#-------------------------------------------------------------------------------
#   2. VALIDATIONS
#-------------------------------------------------------------------------------

echo "SHARED: ${SHARED}"
echo "GROUP_ID: ${GROUP_ID}"
echo "VERSION: ${VERSION}"
echo "GITLAB_GROUP: ${GITLAB_GROUP}"
echo "PACKAGE_BASE: ${PACKAGE_BASE}"

AVC_COMMONS3_DEVELOP=../avc-commons3-develop

if [ ! -d "${AVC_COMMONS3_DEVELOP}" ]; then
	error "avc-commons3-develop should be present: ${AVC_COMMONS3_DEVELOP}"
elif ! echo "${SHARED}" | grep -q -e "-shared$"; then
	error "SHARED should end with '-shared'."
elif [ -z "${GROUP_ID}" ]; then
	error "GROUP_ID should be set. It should be of the form: net.avcompris.examples"
elif [ -z "${VERSION}" ]; then
	error "VERSION should be set. It should be of the form: 0.0.1-SNAPSHOT, or: 0.0.1"
elif [ -z "${GITLAB_GROUP}" ]; then
	error "VERSION should be set. It should be of the form: avcompris, quaefacta, etc."
elif [ -z "${PACKAGE_BASE}" ]; then
	error "VERSION should be set. It should be of the form: net.avcompris.examples, com.qfacta, etc."
fi

package_base_path=`echo "${PACKAGE_BASE}" | sed s/\\\\./\\\\\//g`

#-------------------------------------------------------------------------------
#   3. CREATE DIRS
#-------------------------------------------------------------------------------

mkdir -p "${SHARED}-dao-RDS/src/main/java/${package_base_path}/${SUFFIX}/dao/impl"
mkdir -p "${SHARED}-dao-RDS/src/test/java/${package_base_path}/${SUFFIX}/dao/impl"

mkdir -p "${SHARED}-api/src/main/java/${package_base_path}/${SUFFIX}/api/exception"
mkdir -p "${SHARED}-api/src/test/java/${package_base_path}/${SUFFIX}/api/exception"

mkdir -p "${SHARED}-core-api/src/main/java/${package_base_path}/${SUFFIX}/core/api"
mkdir -p "${SHARED}-core-api/src/test/java/${package_base_path}/${SUFFIX}/core/api"

mkdir -p "${SHARED}-core/src/main/java/${package_base_path}/${SUFFIX}/core/impl"
mkdir -p "${SHARED}-core/src/test/java/${package_base_path}/${SUFFIX}/core/impl"

mkdir -p "${SHARED}-core-tests/src/main/java/${package_base_path}/${SUFFIX}/core/tests"
mkdir -p "${SHARED}-core-tests/src/test/java/${package_base_path}/${SUFFIX}/core/tests"

mkdir -p "${SHARED}-notifier/src/main/java/${package_base}/${SUFFIX}/notifier"
mkdir -p "${SHARED}-notifier/src/test/java/${package_base}/${SUFFIX}/notifier"

#-------------------------------------------------------------------------------
#   4. CREATE FILES
#-------------------------------------------------------------------------------

cp "${AVC_COMMONS3_DEVELOP}/templates/SHARED/.gitignore" .

filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/pom.xml" > pom.xml
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-dao-RDS/pom.xml" > "${SHARED}-dao-RDS/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-api/pom.xml" > "${SHARED}-api/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-core-api/pom.xml" > "${SHARED}-core-api/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-core/pom.xml" > "${SHARED}-core/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-core-tests/pom.xml" > "${SHARED}-core-tests/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/SHARED/SHARED-notifier/pom.xml" > "${SHARED}-notifier/pom.xml"

#-------------------------------------------------------------------------------
#   5. ADD src/main/resources/buildinfo.properties
#-------------------------------------------------------------------------------

ls */pom.xml | sed "s/\\/.*//" | while read module; do
	mkdir -p "${module}/src/main/resources"
	cp "${AVC_COMMONS3_DEVELOP}/templates/COMMON/src/main/resources/buildinfo.properties" "${module}/src/main/resources/"
done

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

echo "Done."

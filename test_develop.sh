#!/bin/sh

# File: avc-commons3-develop/test_develop.sh
#
# Run this script through the command line:
#
#    $ ./src/test/shell/test.sh
#

set -e

#-------------------------------------------------------------------------------
#   1. GIT CLONE: "projects" list
#-------------------------------------------------------------------------------

./git_clone_all.sh

#-------------------------------------------------------------------------------
#   2. GET MAVEN DEPENDENCIES SO WE CAN USE THEM OFFLINE
#-------------------------------------------------------------------------------

./get_maven_dependencies.sh

#-------------------------------------------------------------------------------
#   3. SEQUENTIALLY BUILD PROJECTS FROM avc-commons3-all/pom.xml
#-------------------------------------------------------------------------------

./build_sequentially.sh

#-------------------------------------------------------------------------------
#   4. TEST: "create_shared_subfiles.sh"
#-------------------------------------------------------------------------------

rm -rf ../my-toto-shared

mkdir -p ../my-toto-shared

cd ../my-toto-shared

export GROUP_ID=my.toto
export VERSION=0.0.1-SNAPSHOT
export GITLAB_GROUP=toto
export PACKAGE_BASE=my.toto

../avc-commons3-develop/create_shared_subfiles.sh

mvn -o eclipse:eclipse
mvn -o install

#-------------------------------------------------------------------------------
#   5. TEST: "create_component_subfiles.sh"
#-------------------------------------------------------------------------------

rm -rf ../my-toto-users

mkdir -p ../my-toto-users

cd ../my-toto-users

export GROUP_ID=my.toto
export VERSION=0.0.1-SNAPSHOT
export GITLAB_GROUP=toto
export PACKAGE_BASE=my.toto

../avc-commons3-develop/create_component_subfiles.sh

mvn -o eclipse:eclipse
mvn -o install

mvn -o eclipse:eclipse -f ../my-toto-users-core-it
mvn -o install         -f ../my-toto-users-core-it

mvn -o eclipse:eclipse -f ../my-toto-users-web-inMemory-it
mvn -o install         -f ../my-toto-users-web-inMemory-it

mvn -o eclipse:eclipse -f ../my-toto-users-web-it
mvn -o install         -f ../my-toto-users-web-it

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------


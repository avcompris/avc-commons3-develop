#!/bin/sh

# File: avc-commons3-develop/create_component_subfiles.sh
#
# Parameters:
#    COMPONENT     e.g. avc-examples3-users -- Must be the current directory
#    GROUP_ID      e.g. net.avcompris.commons
#    VERSION       e.g. 0.0.1-SNAPSHOT
#    GITLAB_GROUP  e.g. avcompris
#    PACKAGE_BASE  e.g. net.avcompris.examples.users3
#
# Create the following subdirs and files in a "xxx-xxx" component directory:
#
#   .
#                   pom.xml <-- will inherit from "avc-commons3-component"
#   xxx-xxx-query
#                   src/main/java/${package_base_path}/query
#                   src/test/java/${package_base_path}/query
#                   pom.xml
#   xxx-xxx-dao
#                   src/main/java/${package_base_path}/dao/impl
#                   src/test/java/${package_base_path}/dao/impl
#                   pom.xml
#   xxx-xxx-dao-inMemory
#                   src/main/java/${package_base_path}/dao/impl
#                   src/test/java/${package_base_path}/dao/impl
#                   pom.xml
#   xxx-xxx-dao-RDS
#                   src/main/java/${package_base_path}/dao/impl
#                   src/test/java/${package_base_path}/dao/impl
#                   pom.xml
#   xxx-xxx-api
#                   src/main/java/${package_base_path}/api/exception
#                   src/test/java/${package_base_path}/api/exception
#                   pom.xml
#   xxx-xxx-core-api
#                   src/main/java/${package_base_path}/core/api
#                   src/test/java/${package_base_path}/core/api
#                   pom.xml
#   xxx-xxx-core
#                   src/main/java/${package_base_path}/core/impl
#                   src/test/java/${package_base_path}/core/impl
#                   pom.xml
#   xxx-xxx-core-tests
#                   src/main/java/${package_base_path}/core/tests
#                   src/test/java/${package_base_path}/core/tests
#                   pom.xml
#   xxx-xxx-client
#                   src/main/java/${package_base_path}/client
#                   src/test/java/${package_base_path}/client
#                   pom.xml
#   xxx-xxx-web
#                   src/main/java/${package_base_path}/web
#                   src/test/java/${package_base_path}/web
#                   pom.xml
#
# ... And create the following projects at the same level:
#
#   xxx-xxx-core-it
#                   src/main/java/${package_base_path}/core/it
#                   src/test/java/${package_base_path}/core/it
#                   pom.xml
#
#   xxx-xxx-web-it
#                   src/main/java/${package_base_path}/xxx/web/it
#                   src/test/java/${package_base_path}/xxx/web/it
#                   pom.xml
#

set -e

#-------------------------------------------------------------------------------
#   0. DEFS
#-------------------------------------------------------------------------------

error() {
	message="${1}"
	echo "${message}" >&2
	echo "Exiting." >&2
	exit 1
}

filter_file() {

	subfile_src="${1}"

	cat "${subfile_src}" \
		| sed s/\$\{COMPONENT\}/${COMPONENT}/ \
		| sed s/\$\{SHARED\}/${SHARED}/ \
		| sed s/\$\{GROUP_ID\}/${GROUP_ID}/ \
		| sed s/\$\{VERSION\}/${VERSION}/ \
		| sed s/\$\{GITLAB_GROUP\}/${GITLAB_GROUP}/ \
		| sed s/\$\{PACKAGE_BASE\}/${PACKAGE_BASE}/ \
		| sed s/\$\{SUFFIX\}/${SUFFIX}/
}

#-------------------------------------------------------------------------------
#   2. EXTRACT COMPONENT NAME
#-------------------------------------------------------------------------------

COMPONENT=`basename "${PWD}"`

if [ -z "${COMPONENT}" ]; then
	error "*** ERROR *** Cannot extract COMPONENT name: `pwd`"
fi

SUFFIX=`echo "${COMPONENT}" | sed s/^.*-//`

SHARED=`echo "${COMPONENT}" | sed s/[^-]*$/shared/`

#-------------------------------------------------------------------------------
#   2. VALIDATIONS
#-------------------------------------------------------------------------------

echo "COMPONENT: ${COMPONENT}"
echo "SHARED: ${SHARED}"
echo "GROUP_ID: ${GROUP_ID}"
echo "VERSION: ${VERSION}"
echo "GITLAB_GROUP: ${GITLAB_GROUP}"
echo "PACKAGE_BASE: ${PACKAGE_BASE}"

AVC_COMMONS3_DEVELOP=../avc-commons3-develop

if [ ! -d "${AVC_COMMONS3_DEVELOP}" ]; then
	error "avc-commons3-develop should be present: ${AVC_COMMONS3_DEVELOP}"
elif echo "${COMPONENT}" | grep -q -shared$; then
	error "COMPONENT should not end with '-shared'. It should be of the form: avc-examples-users3"
elif [ -z "${GROUP_ID}" ]; then
	error "GROUP_ID should be set. It should be of the form: net.avcompris.examples"
elif [ -z "${VERSION}" ]; then
	error "VERSION should be set. It should be of the form: 0.0.1-SNAPSHOT, or: 0.0.1"
elif [ -z "${GITLAB_GROUP}" ]; then
	error "VERSION should be set. It should be of the form: avcompris, quaefacta, etc."
elif [ -z "${PACKAGE_BASE}" ]; then
	error "VERSION should be set. It should be of the form: net.avcompris.examples, com.qfacta, etc."
fi

package_base_path=`echo "${PACKAGE_BASE}" | sed s/\\\\./\\\\\//g`

#-------------------------------------------------------------------------------
#   3. CREATE DIRS
#-------------------------------------------------------------------------------

mkdir -p "${COMPONENT}-query/src/main/java/${package_base_path}/${SUFFIX}/query"
mkdir -p "${COMPONENT}-query/src/test/java/${package_base_path}/${SUFFIX}/query"

mkdir -p "${COMPONENT}-dao/src/main/java/${package_base_path}/${SUFFIX}/dao/impl"
mkdir -p "${COMPONENT}-dao/src/test/java/${package_base_path}/${SUFFIX}/dao/impl"

mkdir -p "${COMPONENT}-dao-inMemory/src/main/java/${package_base_path}/${SUFFIX}/dao/impl"
mkdir -p "${COMPONENT}-dao-inMemory/src/test/java/${package_base_path}/${SUFFIX}/dao/impl"

mkdir -p "${COMPONENT}-dao-RDS/src/main/java/${package_base_path}/${SUFFIX}/dao/impl"
mkdir -p "${COMPONENT}-dao-RDS/src/test/java/${package_base_path}/${SUFFIX}/dao/impl"

mkdir -p "${COMPONENT}-api/src/main/java/${package_base_path}/${SUFFIX}/api/exception"
mkdir -p "${COMPONENT}-api/src/test/java/${package_base_path}/${SUFFIX}/api/exception"

mkdir -p "${COMPONENT}-api-tests/src/main/java/${package_base_path}/${SUFFIX}/api/tests"
mkdir -p "${COMPONENT}-api-tests/src/test/java/${package_base_path}/${SUFFIX}/api/tests"

mkdir -p "${COMPONENT}-core-api/src/main/java/${package_base_path}/${SUFFIX}/core/api"
mkdir -p "${COMPONENT}-core-api/src/test/java/${package_base_path}/${SUFFIX}/core/api"

mkdir -p "${COMPONENT}-core/src/main/java/${package_base_path}/${SUFFIX}/core/impl"
mkdir -p "${COMPONENT}-core/src/test/java/${package_base_path}/${SUFFIX}/core/impl"

mkdir -p "${COMPONENT}-core-tests/src/main/java/${package_base_path}/${SUFFIX}/core/tests"
mkdir -p "${COMPONENT}-core-tests/src/test/java/${package_base_path}/${SUFFIX}/core/tests"

mkdir -p "${COMPONENT}-client/src/main/java/${package_base_path}/${SUFFIX}/client"
mkdir -p "${COMPONENT}-client/src/test/java/${package_base_path}/${SUFFIX}/client"

mkdir -p "${COMPONENT}-web/src/main/java/${package_base_path}/${SUFFIX}/web"
mkdir -p "${COMPONENT}-web/src/test/java/${package_base_path}/${SUFFIX}/web"

#-------------------------------------------------------------------------------
#   4. CREATE FILES
#-------------------------------------------------------------------------------

cp "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/.gitignore" .

filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/pom.xml" > pom.xml
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-query/pom.xml" > "${COMPONENT}-query/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-dao/pom.xml" > "${COMPONENT}-dao/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-dao-inMemory/pom.xml" > "${COMPONENT}-dao-inMemory/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-dao-RDS/pom.xml" > "${COMPONENT}-dao-RDS/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-api/pom.xml" > "${COMPONENT}-api/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-api-tests/pom.xml" > "${COMPONENT}-api-tests/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-core-api/pom.xml" > "${COMPONENT}-core-api/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-core/pom.xml" > "${COMPONENT}-core/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-core-tests/pom.xml" > "${COMPONENT}-core-tests/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-client/pom.xml" > "${COMPONENT}-client/pom.xml"
filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-web/pom.xml" > "${COMPONENT}-web/pom.xml"

#-------------------------------------------------------------------------------
#   5. CREATE “-IT” PROJECTS
#-------------------------------------------------------------------------------

mkdir -p "../${COMPONENT}-core-it/src/main/java/${package_base_path}/${SUFFIX}/core/it"
mkdir -p "../${COMPONENT}-core-it/src/test/java/${package_base_path}/${SUFFIX}/core/it/RDS"
mkdir -p "../${COMPONENT}-core-it/src/test/resources"

mkdir -p "../${COMPONENT}-web-inMemory-it/src/main/java/${package_base_path}/${SUFFIX}/web/it"
mkdir -p "../${COMPONENT}-web-inMemory-it/src/test/java/${package_base_path}/${SUFFIX}/web/it"
mkdir -p "../${COMPONENT}-web-inMemory-it/src/test/resources"

mkdir -p "../${COMPONENT}-web-it/src/main/java/${package_base_path}/${SUFFIX}/web/it"
mkdir -p "../${COMPONENT}-web-it/src/test/java/${package_base_path}/${SUFFIX}/web/it"
mkdir -p "../${COMPONENT}-web-it/src/test/resources"

#-------------------------------------------------------------------------------
#   6. “-IT” PROJECTS: CREATE FILES
#-------------------------------------------------------------------------------

cp "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-core-it/.gitignore" \
	"../${COMPONENT}-core-it/"

filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-core-it/pom.xml" \
	> "../${COMPONENT}-core-it/pom.xml"

cp "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-web-inMemory-it/.gitignore" \
	"../${COMPONENT}-web-inMemory-it/"

filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-web-inMemory-it/pom.xml" \
	> "../${COMPONENT}-web-inMemory-it/pom.xml"

cp "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-web-it/.gitignore" \
	"../${COMPONENT}-web-it/"

filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT-web-it/pom.xml" \
	> "../${COMPONENT}-web-it/pom.xml"

#-------------------------------------------------------------------------------
#   7. ADD src/main/resources/buildinfo.properties
#-------------------------------------------------------------------------------

ls */pom.xml | sed "s/\\/.*//" | while read module; do
	mkdir -p "${module}/src/main/resources"
	cp "${AVC_COMMONS3_DEVELOP}/templates/COMMON/src/main/resources/buildinfo.properties" \
		"${module}/src/main/resources/"
done

mkdir -p "../${COMPONENT}-core-it/src/main/resources"

cp "${AVC_COMMONS3_DEVELOP}/templates/COMMON/src/main/resources/buildinfo.properties" \
	"../${COMPONENT}-core-it/src/main/resources/"

mkdir -p "../${COMPONENT}-web-inMemory-it/src/main/resources"

cp "${AVC_COMMONS3_DEVELOP}/templates/COMMON/src/main/resources/buildinfo.properties" \
	"../${COMPONENT}-web-inMemory-it/src/main/resources/"

mkdir -p "../${COMPONENT}-web-it/src/main/resources"

cp "${AVC_COMMONS3_DEVELOP}/templates/COMMON/src/main/resources/buildinfo.properties" \
	"../${COMPONENT}-web-it/src/main/resources/"

#-------------------------------------------------------------------------------
#   8. MAIN CLASS: Application.java
#-------------------------------------------------------------------------------

mkdir -p "${COMPONENT}-web/src/main/java/${package_base_path}/${SUFFIX}/web/"

filter_file "${AVC_COMMONS3_DEVELOP}/templates/COMPONENT/COMPONENT-web/src/main/java/PACKAGE_BASE/SUFFIX/web/Application.java" \
	> "${COMPONENT}-web/src/main/java/${package_base_path}/${SUFFIX}/web/Application.java"

#-------------------------------------------------------------------------------
#   9. END
#-------------------------------------------------------------------------------

echo "Done."

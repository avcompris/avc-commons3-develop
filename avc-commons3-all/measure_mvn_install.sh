#!/bin/sh

# File: avc-commons3-develop/avc-commons3-all/measure_mvn_install.sh

set -e

start=`date`
start_sec=`date +%s`

mvn clean install

end=`date`
end_sec=`date +%s`

echo "  start: ${start}"
echo "    end: ${end}"
echo "elapsed: $(expr $end_sec - $start_sec) sec"

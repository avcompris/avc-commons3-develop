#!/bin/sh

# File: avc-commons3-develop/get_maven_dependencies.sh
#
# Pre-fectch all dependencies listed in avc-commons3-parent/pom.xml.

set -e

cat ../avc-commons3-parent/pom.xml | while read line; do

	if echo "${line}" | grep -q -e \<dependency\> -e \<plugin\>; then

		groupId=
		artifactId=
		version=
		opts=

	elif echo "${line}" | grep -q \<groupId\> && test -z "${groupId}"; then

		groupId=`echo ${line} | sed "s/^[^>]*>//" | sed "s/<.*$//"`

	elif echo "${line}" | grep -q \<artifactId\> && test -z "${artifactId}"; then

		artifactId=`echo ${line} | sed "s/^[^>]*>//" | sed "s/<.*$//"`

	elif echo "${line}" | grep -q \<version\>; then

		version=`echo ${line} | sed "s/^[^>]*>//" | sed "s/<.*$//"`

	elif echo "${line}" | grep -q \<exclusion\>; then

		opts=" -Dtransitive=false"

	elif echo "${line}" | grep -q -e \</dependency\> -e \</plugin\> -e \<dependencies\>; then

		if [ -n "${groupId}" -a -n "${artifactId}" -a "${version}" -a "${artifactId}" != avc-commons3-parent ]; then

			cmd="mvn dependency:get -Dartifact=${groupId}:${artifactId}:${version}${opts}"

			echo "Running: ${cmd}..."

			${cmd}
		fi

	fi

done

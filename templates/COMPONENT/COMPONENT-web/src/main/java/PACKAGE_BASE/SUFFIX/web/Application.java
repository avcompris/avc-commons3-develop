package ${PACKAGE_BASE}.${SUFFIX}.web;

import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import net.avcompris.commons3.client.DataBeanDeserializer;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.notifier.impl.ErrorNotifierInMemory;
import net.avcompris.commons3.utils.ClockImpl;
import net.avcompris.commons3.web.AbstractApplication;
import net.avcompris.commons3.web.ApplicationUtils;

/**
 * A Spring Boot applications.
 */
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@SpringBootApplication
public class Application extends AbstractApplication {

	public static final String NAME = "${COMPONENT}";

	public static final String API = "/api/v1";

	/**
	 * Of the form "${PACKAGE_BASE}.${COMPONENT}".
	 */
	public static final String PACKAGE_PREFIX = substringBeforeLast(Application.class.getPackage().getName(), ".web");

	private static boolean secure = true;

	public static void main(final String... args) throws Exception {

		// 2. SPRING BEANS

		final ApplicationContext context = SpringApplication.run(new Class<?>[] {

				Application.class,

				ClockImpl.class,

				ErrorNotifierInMemory.class, // ErrorNotifierInRabbitMQ.class,

				PermissionsImpl.class,

				SessionPropagator.class,

		}, args);

		ApplicationUtils.dumpBeans(context, System.out, "${PACKAGE_BASE}.${COMPONENT}");
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurerAdapter() {

			@Override
			public void addCorsMappings(final CorsRegistry registry) {

				registry.addMapping("/**").allowedMethods(

						"HEAD", "GET", "POST", "PUT", "DELETE" //
				);
			}
		};
	}

	public static boolean isSecure() {

		final boolean secure = Application.secure;

		if (!secure) {
			System.err.println("*** WARNING *** Application.secure = " + secure
					+ ", which means HTTP Cookies may be sent on non-SSL HTTP connections");
		}

		return secure;
	}

	public static boolean isHttpOnly() {

		return true;
	}

	/**
	 * Because we use interfaces for our DataBean classes (via our DataBeans
	 * utility), we must specify custom JSON deserializers here.
	 */
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer addCustomDeserialization() {

		return new Jackson2ObjectMapperBuilderCustomizer() {

			@Override
			public void customize(final Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder) {

				for (final Class<?> clazz : new Class<?>[] {

				}) {

					jackson2ObjectMapperBuilder.deserializerByType(clazz,
							DataBeanDeserializer.createDeserializer(clazz));
				}
			}
		};
	}
}

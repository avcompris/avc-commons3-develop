#!/bin/sh

# File: avc-commons3-develop/build_sequentially.sh
#
# Individually build each Maven module in avc-commons3-all/pom.xml, and offline;
# We rely on Maven dependencies fetched by get_maven_dependencies.sh

set -e

grep "^\s*<module>" avc-commons3-all/avc-commons3-all-core/pom.xml \
	| sed "s/^[^>]*>//" | sed  "s/<.*//" | while read path; do

	mvn -f "avc-commons3-all/avc-commons3-all-core/${path}" -o clean install

done

mvn -f avc-commons3-all -o clean test-compile

#!/bin/sh

# File: avc-commons3-develop/git_clone_all.sh
#
# Clone all “avc-commons3” projects (public)

set -e

basedir=./
destdir=../

cat "${basedir}/projects" | grep -v ^\s*$ | grep -v ^\s*\# | while read project_name; do

	project_destdir="${destdir}${project_name}"

	if [ -d "${project_destdir}" ]; then
		echo "Skipping: ${project_name}..."
	else
		git_remote_url="https://gitlab.com/avcompris/${project_name}.git"
		git clone "${git_remote_url}" "${project_destdir}"
	fi

done

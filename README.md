# avc-commons3-develop

From this repository,
you can build every “avc-commons3” project this way:

Use a a Git + Maven, environment, for instance:

    $ docker run -ti maven:3.6.3-jdk-8 bash
    # mkdir workspace
    # cd workspace

Clone this very repository:

    $ git clone https://gitlab.com/avcompris/avc-commons3-develop.git
    
Then run:

    $ cd avc-commons3-develop
    $ ./src/test/shell/test_develop.sh
    
This will `git clone` all modules listed in “develop/projects”, then build them:

* non `*-it` projects will be installed;
* `*-it` (integration test) projects will only be compiled; The reason is 
  they rely on external resources such as databases.

## Base Maven Dependencies

![maven-dependencies](doc/diagrams/images/maven-dependencies.png)


